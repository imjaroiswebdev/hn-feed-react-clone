# Simple Hacker News Feed Clone [![pipeline status](https://gitlab.com/imjaroiswebdev/hn-feed-react-clone/badges/master/pipeline.svg)](https://gitlab.com/imjaroiswebdev/hn-feed-react-clone/-/commits/master)

This a project develop to be deployed with Docker Compose, so the included `docker-compose.yml` file included in this repository will allow to start the complete Project including its DB for in Production mode.

## Start the project for Production Environment

Being in the repository root execute:
`docker-compose up`

For starting the project in a Development Environment you will need to override the actual Docker Compose configuration. So here is the content of the `docker-compose.override.yml` file for that manner.

## docker-compose.override.yml (Development)

```yaml
version: "3.6"
services:
  backend:
    command: npm run dev

  frontend:
    build:
      dockerfile: Dockerfile.dev
    command: npm run start
```

## docker-compose.override.yml (Runnig Project Tests)

```yaml
version: "3.6"
services:
  backend:
    command: npm test

  frontend:
    build:
      dockerfile: Dockerfile.dev
    command: npm test
```

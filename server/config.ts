import { env } from "process";

export const config = {
  PORT_APP: Number(env.SERVER_PORT),
  HN_FEED_API_PATH: env.HN_FEED_API_PATH,
  HN_FEED_SCHEDULE_CRON_EXP: env.HN_FEED_SCHEDULE_CRON_EXP,
  MONGO_CONNECTION_STRING: env.MONGO_CONNECTION_STRING,
  MONGO_DB_NAME: env.MONGO_DB_NAME,
  MONGO_COLLECTION_NAME: env.MONGO_COLLECTION_NAME,
};

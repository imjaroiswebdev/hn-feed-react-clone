import * as chai from "chai";
import * as path from "path";
import { Server } from "../config/Server";
import { Service } from "../app/services/Index";
// tslint:disable-next-line: no-var-requires
require("dotenv").config({ path: `${path.resolve()}/.env` });

const server: Server = new Server();

describe("App Service", () => {
  before(done => {
    server.Start().then(() => done());
  });

  it("Schedule Entries Retriever Job Succesfuly", done => {
    const apiService = new Service(server.db);

    try {
      apiService.scheduleEntriesRetrieving();
      const lastExecDate = apiService.getEntriesRetrievingJob.lastDate();
      chai.expect(new Date().getDay()).to.eq(lastExecDate.getDay());

      done();
    } catch (err) {
      done(err);
    }
  });

  it("Retrieved entries are saved to the db", done => {
    const apiService = new Service(server.db);

    apiService
      .retrieveEntries()
      .then(() => {
        const lastRetreivedEntries: [] = apiService.getLastRetreivedEntries;
        const lastTotalEntriesSaved: number =
          apiService.getLastTotalEntriesSaved;
        chai.expect(lastRetreivedEntries).to.be.a("array");
        chai.expect(lastTotalEntriesSaved).to.be.a("number");

        done();
      })
      .catch(err => done(err));
  });
});

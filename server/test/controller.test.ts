import * as chai from "chai";
import * as path from "path";
import * as supertest from "supertest";
import { IExpressAppWithDB } from "../app/interfaces/IExpressAppWithDB";
import { Server } from "../config/Server";
import { Service } from "../app/services/Index";
// tslint:disable-next-line: no-var-requires
require("dotenv").config({ path: `${path.resolve()}/.env` });

const server: Server = new Server();
let app: IExpressAppWithDB;

describe("App Controller", () => {
  before(done => {
    server.Start().then(async () => {
      app = server.App();
      const db = server.db;
      const apiService = new Service(db);

      try {
        await apiService.retrieveEntries();
        done();
      } catch (err) {
        done(err);
      }
    });
  });

  it("GET: entries 200", done => {
    supertest(app)
      .get("/api/entries")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .end((err: Error, res: supertest.Response) => {
        if (err) {
          done(err);
        }

        chai.expect(res.body.status.code).to.be.a("number");
        chai.expect(res.body.status.code).to.eq(200);
        chai.expect(res.body.data.entries).to.be.a("array");
        done();
      });
  });

  it("PUT: entry 200", done => {
    const objectID = "22297569";
    supertest(app)
      .put("/api/entry/" + objectID)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .end((err: Error, res: supertest.Response) => {
        if (err) {
          done(err);
        }

        chai.expect(res.body.status.code).to.be.a("number");
        chai.expect(res.body.status.code).to.eq(200);
        done();
      });
  });

  it("PUT: entry 404", done => {
    const objectID = "7324234049";
    supertest(app)
      .put("/api/entry/" + objectID)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(404)
      .end((err: Error, res: supertest.Response) => {
        if (err) {
          done(err);
        }

        chai.expect(res.body.status.code).to.be.a("number");
        chai.expect(res.body.status.code).to.eq(404);
        chai.expect(res.body.status.description).to.be.a("string");
        chai
          .expect(res.body.status.description)
          .to.eq("Story not found for objectID: " + objectID);
        done();
      });
  });

  it("healthz 200", done => {
    supertest(app)
      .get("/api/healthz")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .end((err: Error, res: supertest.Response) => {
        chai.expect(res.body.status.code).to.be.a("number");
        chai.expect(res.body.status.code).to.eq(200);
        chai.expect(res.body.data.serverTime).to.be.a("string");
        done();
      });
  });

  it("Unkwon routes 404", done => {
    supertest(app)
      .get("/api/whatever")
      .set("Accept", "application/json")
      .expect(404)
      .end((err: Error, res: supertest.Response) => {
        chai.expect(res.body.status.code).to.be.a("number");
        chai.expect(res.body.status.code).to.eq(404);
        // tslint:disable-next-line: no-unused-expression
        chai.expect(res.body.data).to.be.null;
        done();
      });
  });
});

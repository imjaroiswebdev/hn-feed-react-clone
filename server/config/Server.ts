import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as express from "express";
import * as http from "http";
import * as morgan from "morgan";
import { config } from "../config";
import { ROUTER } from "./Router";
import { IResponse } from "../app/interfaces/IResponse";
import { IThrownError } from "../app/interfaces/IThrownError";
import { IExpressAppWithDB } from "../app/interfaces/IExpressAppWithDB";
import { Db as MongoDB, MongoClient } from "mongodb";

export class Server {
  private readonly app: IExpressAppWithDB;
  private readonly server: http.Server;

  constructor() {
    this.app = express();
    this.server = http.createServer(this.app);
  }

  public async Start(): Promise<http.Server> {
    await this.DBConfiguration();
    this.ExpressConfiguration();
    this.ConfigurationRouter();
    return this.server;
  }

  public App(): IExpressAppWithDB {
    return this.app;
  }

  public get db(): MongoDB {
    return this.app.db;
  }

  private async DBConfiguration(): Promise<void> {
    const mongoClient = new MongoClient(config.MONGO_CONNECTION_STRING, {
      useUnifiedTopology: true,
    });
    try {
      const connectionClient: MongoClient = await mongoClient.connect();
      const db: MongoDB = connectionClient.db(config.MONGO_DB_NAME);
      const collection = db.collection(config.MONGO_COLLECTION_NAME);
      await collection.createIndex({ objectID: 1 }, { unique: true }); // For making the entries uniques

      this.app.db = db;
    } catch (err) {
      throw err;
    }
  }

  private ExpressConfiguration(): void {
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json({ limit: "50mb" }));
    this.app.use((req, res, next): void => {
      res.header("Access-Control-Allow-Origin", "*");
      res.header(
        "Access-Control-Allow-Headers",
        "X-Requested-With, Content-Type, Authorization"
      );
      res.header(
        "Access-Control-Allow-Methods",
        "GET,PUT,PATCH,POST,DELETE,OPTIONS"
      );
      next();
    });
    this.app.use(morgan("combined"));
    this.app.use(cors());
    this.app.use(
      (
        err: IThrownError,
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
      ): void => {
        err.status = 404;
        next(err);
      }
    );
  }

  private ConfigurationRouter(): void {
    for (const route of ROUTER) {
      this.app.use(route.path, route.middleware, route.handler);
    }
    this.app.use(
      (
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
      ): void => {
        const errorResponse: IResponse = {
          data: null,
          status: {
            code: 404,
            description: "Not found",
            techCode: "",
          },
        };
        res.status(404).json(errorResponse);
        next();
      }
    );
    this.app.use(
      (
        err: IThrownError,
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
      ): void => {
        const code: number = err.status || 500;
        const errorResponse: IResponse = {
          data: null,
          status: {
            code,
            description: err.message,
            techCode: "",
          },
        };
        res.status(err.status || 500).json(errorResponse);
        next();
      }
    );
  }
}

import * as express from "express";
import { ApiRouter } from "../app/routes/Index";

interface IROUTER {
  path: string;
  middleware: any[];
  handler: express.Router;
}

const API = new ApiRouter();

export const ROUTER: IROUTER[] = [
  {
    handler: API.router,
    middleware: [],
    path: "/api",
  },
];

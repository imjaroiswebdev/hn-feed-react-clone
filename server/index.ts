import { env } from "process";
import { config } from "./config";
import { Server } from "./config/Server";
import { Service } from "./app/services/Index";

console.log(`\n -------------------> RUN ${env.NODE_ENV} ENVIRONMENT \n`);

(async () => {
  const port: number = Number(env.PORT) || config.PORT_APP || 8080;
  const server = new Server();
  const httpServer = await server.Start();
  httpServer.listen(port);
  httpServer.on("error", (error: any) => {
    if (error.syscall !== "listen") {
      throw error;
    }
    switch (error.code) {
      case "EACCES":
        console.error("Port requires elevated privileges");
        process.exit(1);
        break;
      case "EADDRINUSE":
        console.error("Port is already in use");
        process.exit(1);
        break;
      default:
        throw error;
    }
  });
  httpServer.on("listening", async () => {
    console.log(
      "Server is running in process " +
        process.pid +
        " listening on PORT " +
        port +
        "\n"
    );
  });
  httpServer.on("listening", () => {
    const apiService = new Service(server.db);
    try {
      apiService.scheduleEntriesRetrieving();
    } catch (err) {
      throw err;
    }
  });
})();

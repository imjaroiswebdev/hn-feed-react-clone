import * as express from "express";
import { IExpressAppWithDB } from "./IExpressAppWithDB";

export interface IExpressReqWithDB extends express.Request {
  app: IExpressAppWithDB;
}

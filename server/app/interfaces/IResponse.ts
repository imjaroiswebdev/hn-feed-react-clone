import { IResStatus } from "./IResStatus";

export interface IResponse {
  status?: IResStatus;
  data?: any;
}

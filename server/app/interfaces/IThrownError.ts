export interface IThrownError extends Error {
  status?: number;
}

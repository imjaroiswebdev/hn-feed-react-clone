export interface IEntriesRepo {
  saveEntries([]): Promise<{ insertedCount: number }>;
  list(query?: { isDeleted: boolean }): Promise<[]>;
  markAsDeleted(
    objectID: string
  ): Promise<{ matchedCount: number; modifiedCount: number }>;
}

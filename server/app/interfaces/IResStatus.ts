export interface IResStatus {
  code?: number;
  description?: string;
  techCode?: string;
}

import * as express from "express";
import { Db as MongoDB } from "mongodb";

export interface IExpressAppWithDB extends express.Application {
  db?: MongoDB;
}

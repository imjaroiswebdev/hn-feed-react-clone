import { IResponse } from "../interfaces/IResponse";
import { IExpressReqWithDB } from "../interfaces/IExpressReqWithDB";
import * as express from "express";

export abstract class Controller {
  protected req: IExpressReqWithDB;
  protected res: express.Response;

  constructor(req: IExpressReqWithDB, res: express.Response) {
    this.req = req;
    this.res = res;
  }

  protected response(payload: IResponse, code: number = 200): express.Response {
    const response = this.res.status(code).send({
      data: { ...payload.data },
      status: { code, description: "", techCode: "", ...payload.status },
    });
    return response;
  }
}

import { Collection as MongoCollection, Db as MongoDB } from "mongodb";
import { config } from "../../config";
import { IEntriesRepo } from "../interfaces/IEntriesRepo";

export class EntriesRepository implements IEntriesRepo {
  private collection: MongoCollection;

  constructor(db: MongoDB) {
    this.collection = this.collectionProvider(db);
  }

  public saveEntries(entries: []): Promise<{ insertedCount: number }> {
    return new Promise(async (resolve, reject) => {
      this.collection.insertMany(entries, { w: 0 }, (err, result) => {
        // BulkWriteError: E11000 duplicate key error collection
        if (err && err.code !== 11000) {
          reject(err);
        } else {
          resolve({ insertedCount: result.ops.length });
        }
      });
    });
  }

  public list(query?: { isDeleted: boolean }): Promise<[]> {
    return new Promise(async (resolve, reject) => {
      this.collection
        .find(query)
        .sort({ created_at: -1 })
        .toArray()
        .then((entries: []) => resolve(entries))
        .catch(err => reject(err));
    });
  }

  public markAsDeleted(
    objectID: string
  ): Promise<{ matchedCount: number; modifiedCount: number }> {
    return new Promise(async (resolve, reject) => {
      this.collection.updateOne(
        { objectID },
        { $set: { isDeleted: true } },
        (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve({
              matchedCount: result.matchedCount,
              modifiedCount: result.modifiedCount,
            });
          }
        }
      );
    });
  }

  private collectionProvider(mongoDB: MongoDB): MongoCollection {
    return mongoDB.collection(config.MONGO_COLLECTION_NAME);
  }
}

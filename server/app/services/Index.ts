import { CronJob } from "cron";
import { Db as MongoDB } from "mongodb";
import * as request from "request-promise";
import { config } from "../../config";
import { IEntriesRepo } from "../interfaces/IEntriesRepo";
import { EntriesRepository } from "../repositories/Repository";

export class Service {
  private HN_FEED_API_PATH: string;
  private entriesRepository: IEntriesRepo;
  private CRON_EXPRESSION: string;
  private entriesRetrievingJob: CronJob;
  private lastRetreivedEntries: any;
  private lastTotalEntriesSaved: number;

  constructor(db: MongoDB) {
    this.HN_FEED_API_PATH = config.HN_FEED_API_PATH;
    this.CRON_EXPRESSION = config.HN_FEED_SCHEDULE_CRON_EXP;
    this.entriesRepository = this.entriesRepoProvider(db);
    this.lastRetreivedEntries = [];
    this.lastTotalEntriesSaved = 0;
  }

  public get getEntriesRetrievingJob(): CronJob {
    return this.entriesRetrievingJob;
  }

  public get getLastRetreivedEntries(): [] {
    return this.lastRetreivedEntries;
  }

  public get getLastTotalEntriesSaved(): number {
    return this.lastTotalEntriesSaved;
  }

  public listEntries(): Promise<[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const entryList = await this.entriesRepository.list({
          isDeleted: false,
        });
        resolve(entryList);
      } catch (err) {
        reject(err);
      }
    });
  }

  public markAsDeleted(
    objectID: string
  ): Promise<{ matchedCount: number; modifiedCount: number }> {
    return new Promise(async (resolve, reject) => {
      try {
        const updateResult = await this.entriesRepository.markAsDeleted(
          objectID
        );
        resolve(updateResult);
      } catch (err) {
        reject(err);
      }
    });
  }

  public scheduleEntriesRetrieving(): void {
    const apiServiceContext = this;
    try {
      const cronJob = new CronJob({
        cronTime: this.CRON_EXPRESSION,
        onTick: this.retrieveEntries,
        start: true,
        runOnInit: true,
        context: apiServiceContext,
      });
      this.entriesRetrievingJob = cronJob;
    } catch (err) {
      throw new Error(
        `La expresión CRON "${this.CRON_EXPRESSION}" para programar el servicio es inválida`
      );
    }
  }

  public async retrieveEntries(): Promise<void> {
    let retreivedEntries: { hits: [] };
    try {
      retreivedEntries = await request.get(this.HN_FEED_API_PATH, {
        json: true,
      });
    } catch (err) {
      console.log("Error while retrieving Hackers News entries::>");
      throw err;
    }

    if (!retreivedEntries?.hits?.length) {
      console.log(
        "None Hacker News was retreived...",
        new Date().toLocaleDateString()
      );
    } else {
      try {
        const entries = retreivedEntries.hits.map((entry: object) => ({
          ...entry,
          isDeleted: false,
        }));
        this.lastRetreivedEntries = entries;
        const { insertedCount } = await this.entriesRepository.saveEntries(
          entries
        );
        console.log("HN Entries saved at::>", new Date().toLocaleString());
        this.lastTotalEntriesSaved = insertedCount;
      } catch (err) {
        console.log("Error while saving Hackers News entries::>");
        throw err;
      }
    }
  }

  private entriesRepoProvider(db: MongoDB): IEntriesRepo {
    return new EntriesRepository(db);
  }
}

import { Router } from "../helpers/Router";
import { ApiController } from "../controllers/Index";

export class ApiRouter extends Router {
  constructor() {
    super(ApiController);
    this.router
      .get("/entries", this.handler(this.Controller.listEntries))
      .put("/entry/:objectID", this.handler(this.Controller.markAsDeleted))
      .get("/healthz", this.handler(this.Controller.healthz));
  }
}

import { Request, Response } from "express";
import { Db as MongoDB } from "mongodb";
import { Controller } from "../helpers/Controller";
import { Service } from "../services/Index";
import { IExpressReqWithDB } from "../interfaces/IExpressReqWithDB";

export class ApiController extends Controller {
  private service: Service;

  constructor(req: IExpressReqWithDB, res: Response) {
    super(req, res);
    this.service = new Service(req.app.db);
  }

  public async listEntries() {
    try {
      const entryList: [] = await this.service.listEntries();
      this.response({ data: { entries: entryList } }, 200);
    } catch (err) {
      console.error(err);
      this.response({ data: null }, 500);
    }
  }

  public async markAsDeleted() {
    const objectID: string = this.req.params.objectID;
    try {
      const updateResult: {
        matchedCount: number;
        modifiedCount: number;
      } = await this.service.markAsDeleted(objectID);

      const isFound = updateResult.matchedCount > 0;
      if (!isFound) {
        this.response(
          {
            data: null,
            status: {
              description: "Story not found for objectID: " + objectID,
            },
          },
          404
        );
      } else {
        this.response({ data: null }, 200);
      }
    } catch (err) {
      console.error(err);
      this.response({ data: null }, 500);
    }
  }

  public healthz(): void {
    const serverTime = new Date().toLocaleString();
    this.response({
      data: { serverTime },
      status: { description: "Healthy" },
    });
  }
}

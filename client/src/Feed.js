import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import { FixedSizeList } from "react-window";
import AutoSizer from "react-virtualized-auto-sizer";

const genStyles = makeStyles(theme => ({
  root: {
    paddingTop: 30,
    width: "100%",
    height: "100vh",
    backgroundColor: theme.palette.background.paper
  },
  inline: {
    display: "inline",
    paddingRight: 30
  },
  listItem: {
    paddingLeft: 20,
    borderBottom: 1,
    borderBottomStyle: "solid",
    borderBottomColor: "#ccc",
    marginLeft: 30
    // marginRight: 50,
  },
  storyTitle: {
    fontSize: 13
  },
  author: {
    color: "#999",
    marginLeft: 10
  }
}));

function renderRow(deleteHandler) {
  return function(props) {
    const classes = genStyles();
    const { index, data, style } = props;
    const { storyTitle, author, storyUrl, objectID, createdAt } = data[index];

    return (
      <ListItem button className={classes.listItem} key={index} style={style}>
        <React.Fragment>
          <ListItemText
            primary={
              <React.Fragment>
                <Typography
                  component="span"
                  variant="body2"
                  className={classes.storyTitle}
                  color="textPrimary"
                >
                  {storyTitle + "."}
                </Typography>

                <Typography
                  component="span"
                  variant="body2"
                  className={classes.author}
                  color="textPrimary"
                >
                  {`- ${author} -`}
                </Typography>
              </React.Fragment>
            }
            onClick={() => {
              if (!storyUrl) {
                return alert("No url to entry available 😟");
              }

              window.open(storyUrl, "BLANK");
            }}
          />
          <ListItemSecondaryAction>
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                {createdAt}
              </Typography>
              <IconButton
                edge="start"
                style={{ marginRight: 30 }}
                aria-label="delete"
                onClick={deleteHandler(objectID)}
              >
                <DeleteIcon />
              </IconButton>
            </React.Fragment>
          </ListItemSecondaryAction>
        </React.Fragment>
      </ListItem>
    );
  };
}

renderRow.propTypes = {
  index: PropTypes.number.isRequired,
  style: PropTypes.object.isRequired
};

export function Feed({ entries, deleteHandler }) {
  const classes = genStyles();

  return (
    <div className={classes.root}>
      {!entries.length ? (
        <p>There are no entries to display</p>
      ) : (
        <AutoSizer>
          {({ height, width }) => (
            <FixedSizeList
              height={height}
              width={width}
              itemSize={60}
              itemCount={entries.length}
              itemData={entries}
            >
              {renderRow(deleteHandler)}
            </FixedSizeList>
          )}
        </AutoSizer>
      )}
    </div>
  );
}

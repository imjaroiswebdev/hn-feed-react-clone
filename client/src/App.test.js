import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

test("renders HN Feed header", () => {
  const { getByText } = render(<App />);
  const headerElement = getByText(/HN Feed/i);
  expect(headerElement).toBeInTheDocument();
});

import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import moment from "moment";
import { Feed } from "./Feed";
import "./App.css";

moment.updateLocale("en", {
  calendar: {
    lastDay: "[Yesterday]",
    sameDay: "hh:mm a",
    nextDay: "[Tomorrow at] LT",
    lastWeek: "[last] dddd",
    nextWeek: "dddd [at] LT",
    sameElse: "MMM DD"
  }
});

class App extends React.Component {
  state = {
    entries: [],
    isLoading: true
  };

  componentDidMount() {
    this.getEntries();
  }

  getEntries() {
    this.setState({ isLoading: true }, () => {
      fetch("http://localhost:8080/api/entries")
        .then(response => {
          if (response.ok && response.status === 200) {
            return response.json();
          }
          throw new Error(response.statusText);
        })
        .then(payload => {
          const entries = payload.data.entries
            .map(entry => {
              if (!entry) {
                return null;
              }

              const storyTitle = entry.story_title || entry.title;
              if (!storyTitle) {
                return null;
              }
              const author = entry.author || "";
              const createdAt = moment(entry.created_at).calendar();
              const storyUrl = entry.story_url || entry.url;
              const objectID = entry.objectID;

              return { storyTitle, author, storyUrl, objectID, createdAt };
            })
            .filter(entry => !!entry);

          this.setState({ entries, isLoading: false });
        })
        .catch(err => {
          console.log(err);
          this.setState({ entries: [], isLoading: false });
        });
    });
  }

  deleteEntry = objectID => event => {
    fetch("http://localhost:8080/api/entry/" + objectID, {
      method: "PUT"
    })
      .then(response => {
        if (response.ok && response.status === 200) {
          return response.json();
        }
        throw new Error(response.statusText);
      })
      .then(() => {
        this.getEntries();
      })
      .catch(err => {
        console.log(err);
        alert("An error ocurred and the entry couldn`t be deleted.");
      });
  };

  render() {
    return (
      <div className="App">
        <header className="header">
          <h1 className="heading-text">HN Feed</h1>
          <p className="sub-heading-text">{"We <3 hacker news!"}</p>
        </header>

        {this.state.isLoading ? (
          <CircularProgress />
        ) : (
          <Feed entries={this.state.entries} deleteHandler={this.deleteEntry} />
        )}
      </div>
    );
  }
}

export default App;
